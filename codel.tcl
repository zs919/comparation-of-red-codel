#Create a simulator object
set ns [new Simulator]
set rep [lindex $argv 0]

set rng1 [new RNG]

for {set i 1} {$i<$rep} {incr i} {
        $rng1 next-substream;
       
}


#Define different colors for data flows (for NAM)
$ns color 1 Blue
$ns color 2 Red
#Open the NAM trace file
set nf [open out.nam w]
$ns namtrace-all $nf
set nd [open out.tr w]
$ns trace-all $nd
#Define a 'finish' procedure
proc finish {} {
global ns nf nd
$ns flush-trace
close $nf
close $nd
exec nam out.nam &
exit 0
}

Queue/CoDel set target_ 5
Queue/CoDel set interval_ 5

set lambda 1300.0

#Create four nodes
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
#Create links between the nodes
$ns duplex-link $n1 $n2 500Mb 20ms DropTail
set link [$ns duplex-link $n2 $n3 3Mb 20ms CoDel]
#Set Queue Size of link (n2-n3) to 100
$ns queue-limit $n2 $n3 100
#Give node position (for NAM)
$ns duplex-link-op $n1 $n2 orient right-up
$ns duplex-link-op $n2 $n3 orient right
#Monitor the queue for link (n2-n3). (for NAM)
$ns duplex-link-op $n2 $n3 queuePos 0.5

# generate random interarrival times and packet sizes
set InterArrivalTime [new RandomVariable/Exponential]
$InterArrivalTime use-rng $rng1
$InterArrivalTime set avg_ [expr 1/$lambda]

#Setup a UDP connection
set udp [new Agent/UDP]
$udp set packetSize_ 1000000
$ns attach-agent $n1 $udp

set null [new Agent/Null]
$ns attach-agent $n3 $null
$ns connect $udp $null
$udp set fid_ 2

proc sendpacket {} {
    global ns udp InterArrivalTime pktSize
    set time [$ns now]
    $ns at [expr $time + [$InterArrivalTime value]] "sendpacket"
    set bytes 512
    $udp send $bytes
}

#Schedule events for the CBR agents
$ns at 0.1 "sendpacket"
$ns at 50.0 "finish"

# queue monitoring
set qmon [$ns monitor-queue $n2 $n3 [open qm.out w] 0.1]
[$ns link $n2 $n3] queue-sample-timeout
#Run the simulation
$ns run