# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The experiment is to comparation the performance of RED and CoDel. The origin paper is: Kathleen Nichols and Van Jacobson. 2012. Controlling queue delay. Communications of the ACM. 55, 7 (July 2012), 42-50. Available http://doi.acm.org/10.1145/2209249.2209264. Due to the complicated install process of ns-2simulator (even has some mistake in it) by the author, and the large data files needed to download from the terminals (each one may about 800M), the experiment will cost about 3 hours.

### Background ###

* RED algorithm

RED, named Random Early Detect, is an algorithm to control the queue delay. Then algorithm will continuously detect the average queue length. When the queue length is smaller than a down threshold it will do nothing. When the queue length is larger than the down threshold, it will randomly discard packet by probability P, which linearly depends on the queue length. Finally when the queue length is bigger than a up threshold P will equal to 1 and discard all coming packets.

* CoDel algorithm

CoDel, named Controlled Delay Active Queue Management. The algorithm has several step:
Frist, every packet will get a mark, which tells the waiting time of the packets,t.
Then, when t is bigger than a threshold, named target, it will get another time mark, interval.
Next, when t is equal to (target + interval), if the packet is still in queue, discard the packet, and let d be smaller.
Finally, When some packets dequeue in time (target + interval), refresh interval to the origin value.

* Comparation between the two algorithm
Since RED uses the average queue length, so it will have more parameters. For example, the time interval to take average, the weight of the last average queue length, etc. This parameter need to be modified under different conditions. Furthermore, no matter how explicit parameter you design, the average queue length will still some delay with the instant queue length, so RED will have a low performance on mini burst traffic.

### Result ###

* The picture I want to reproduce

![111.png](https://bitbucket.org/repo/eBERMp/images/1997627869-111.png)

Kathleen Nichols and Van Jacobson. 2012. Controlling queue delay. Communications of the ACM. 55, 7 (July 2012), 42-50. Available http://doi.acm.org/10.1145/2209249.2209264

* What I have reproduced

![222.png](https://bitbucket.org/repo/eBERMp/images/4111506566-222.png)

![333.png](https://bitbucket.org/repo/eBERMp/images/1480244807-333.png)

* comment about my reproduced plot

Since the author doesn’t give me enough parameters’ value. I can’t reproduce the pictures which are totally the same. But the performance of CoDel and RED are showed will. From the Delay of RED we can see that the delay is linearly related to the bandwidth (Since RED fixed the queue length, the larger the bandwidth, the smaller the delay). From the Delay of CoDel, we can see that the delay of different bandwidth is similar (since CoDel fix the time of every packets waiting in queue, the delay will be the same.) So the left two picture make sure that my simulator is right, also, the variance of CoDel is about 3 times smaller than RED’s.  We can also find out that for the Link Utilization the variance of the RED is much bigger than CoDel. These are just the origin picture want to show : CoDel is better than RED in high bandwidth.

The only thing need to improve is that I just simulate from 1Mb to 10Mb in Geni. Because when I want to send packets in a speed higher than 5000/s its actual speed is always much lower. (For example 100Mb may just be 20Mb when I run the Geni).

### Run my experiment ###

####Geni####

1.Add resources

Login to geni and add resources like the following picture，Rspec file is also avaliable in the source.

![444.png](https://bitbucket.org/repo/eBERMp/images/1915600630-444.png)


2.Install software and node c and node s

>sudo apt-get update # refresh local information about software repositorie

>sudo apt-get install d-itg # install D-ITG software package


3.Upload script queuemonitor.sh to node r, run : chmod a+x queuemonitor.sh to active it

4.Start the experiment

  * At node s run: ITGRecv to listen the link
  * At node r set the link
    * For RED
>sudo tc qdisc del dev eth2 root
>
>sudo tc qdisc replace dev eth2 root handle 1: tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 4800
>
>sudo tc qdisc add dev eth2 parent 1: handle 2: red limit 256000 min 2560 max 5120 avpkt 512 burst 7 probability 1 bandwidth 1024
    * For CoDel
> sudo tc qdisc del dev eth2 root
>
> sudo tc qdisc replace dev eth2 root handle 1: tbf rate 1mbit limit 200mb burst 32kB peakrate 1.01mbit mtu 4800
>
> sudo tc qdisc add dev eth2 parent 1: handle 2: codel target 10ms interval 5ms
    * When you need to change the bandwidth of the link, for example, change 1Mb to 3Mb,for red, just let : tbf rate 1mbit->3mbit, burst 32kb->96kb, peakrate 1.01mbit -> 3.03mbit bandwidth 1024 -> 3072. For CoDel just let : tbf rate 1mbit->3mbit, burst 32kb->96kb, peakrate 1.01mbit -> 3.03mbit
  * At the node c run
>ITGSend -a s -l sender.log -x receiver.log -E 433 -d 512 –t 60000 -T UDP to start send packets
  * When you need to change the input speed of the link, for example, change 1Mb to 3Mb just change 433 to 433*3=1300, and so on.
  * For node r After several second, run
>./queuemonitor.sh eth2 40 0.1 | tee ***.csv
  * Redo up 3 steps 5 times to get enough data
  * Change the bandwidth from 1Mbit, 3Mbit, 5Mbit, 10Mbit redo again
  * Download the data and use Excel to process it and make picture (will cover it in NS2). The data from likes the following picture

![55.png](https://bitbucket.org/repo/eBERMp/images/2372045446-55.png)

We can use backlog to calculate the delay. (just use little’s law to transform queue length to delay, and use Excel to plot box plot ) Use sent ***** bytes and the time to get the utilization of the link.(ρ=(sent2-sent1)*8/(t2-t1)/bandwidth)



####NS2####

1.Install software

  * Fully Install ns-2 simulator

    * Run : wget http://iweb.dl.sourceforge.net/project/nsnam/ns-2/2.35/ns-src-2.35.tar.gz    to get the ns2 file

    * Run : sudo apt-get install tar # install rat software package, an unzip software

    * Run tar –xzvf ns-allinone* to untar it

    * Run wget http://www.pollere.net/CoDel-ns-2.35.patch  #Get the patch

    * Cd into ns-allinone-2.35, run patch -p1 < CoDel-ns-2.35.patch     to patch the file

    * vi.Cd into ns-allinone-2.35/ns-2.35/linkstate/ open ls.h change the 137st line, erase change to this->rease  if it doesn’t work, change to void eraseAll() { this->erase(baseMap::begin(), baseMap::end()); }  # something wrong in the install file

    * Open .bashrc, attach # add path for ns2 
export PATH="$PATH:/home/path/ns-allinone-2.35/bin:/home/path/ns-allinone-2.35/tcl8.5.10/unix:/home/path/ns-allinone-2.35/tk8.5.10/unix"
to the end of the file


    * Then build ns-2 normally (see its README)

  * Install nam

    * cd into ns-allinone-2.35/nam-1.15
    * Sudo ./configure    then    sudo make    then   sudo install


2. Upload red.tcl and codel.tcl to the terminal.

3. Run ns red.tcl ** # a random number for RED / Run ns codel.tcl **#random number for CoDel

4. The output file is qm.out and out.tr. The form of qm.out is : Time, From node, To node, Queue size (B), Queue size (packets), Arrivals (packets), Departures (packets), Drops (packets), Arrivals (bytes), Departures (bytes), Drops (bytes)

So we can easily get the delay.

From the out.tr, we can get the link utilization


5. Open red.tcl or codel.tcl change lambda from 1300, 4333, 19500, 43333 and link capacity of n2 and n3 from 3Mb,10Mb,45Mb,100Mb. Each of them run 5 times with different random number to get enough data

6. Use Excel to plot picture

Note1:When you successfully run the script, you will get a nam video likes the following picture:

![666.png](https://bitbucket.org/repo/eBERMp/images/4005847402-666.png)

Note2:How to process data using Excel.

For excample, you can change the name of out.tr to out.csv and open it by Excel, what you get will like the following picture:

![777.png](https://bitbucket.org/repo/eBERMp/images/1647036192-777.png)

Now we need the dequeue packet to calculate the link utilization. So we use selection to choose the line with the head “-”, what we get is ：

![888.png](https://bitbucket.org/repo/eBERMp/images/3208979771-888.png)

Then we can divide each line by space, what we get is

![999.png](https://bitbucket.org/repo/eBERMp/images/997446721-999.png)

Then you can easily calculate the output rate

Note3:
The experiment just uses the default version of geni, and use instageni aggregate.

Note4:
The experiment uses ns-allione2.35 and nam.

Note5:
When I want to let geni send packet 43333 per second, it just send aroud 3000 packet perscend. So I havn’t simulator the high bandwidth on geni.